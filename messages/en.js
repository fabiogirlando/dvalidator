export default {
  names: {
    username: 'usernameeeeee',
    email: 'email',
    numberField: 'number',
    profile: 'profile image',
  },
  messages: {
    required: {
      default: 'Your {fieldName} is required',
    },
    alpha: {
      default: 'Your {fieldName} can only contain alphabets'
    },
    alphaNumeric: {
      default: 'Your {fieldName} can only contain alphabets and numbers'
    },
    numeric: {
      default: 'Your {fieldName} can only contain numbers'
    },
    email: {
      default: 'Your {fieldName} is not a valid email'
    },
    ext: {
      default: 'The extension of your {fieldName} can only be {ext}',
      profile: 'You have upload files of correct types - {ext}'
    },
    max: {
      default: 'Your {fieldName} cannot be bigger than {max}'
    },
    min: {
      default: 'Your {fieldName} cannot be smaller than {min}'
    },
    maxLength: {
      default: 'Your {fieldName} cannot be longer than {maxLength} characters'
    },
    minLength: {
      default: 'Your {fieldName} cannot be shorter than {minLength} characters'
    },
    maxSize: {
      default: 'Your {fieldName} cannot be larger than {maxSize}MB'
    },
    minSize: {
      default: 'Your {fieldName} cannot be smaller than {minSize}MB'
    },
    passwordConfirm: {
      default: 'Your passwords do not match'
    }
  }
}