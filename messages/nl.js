export default {
  names: {
    username: 'username',
    email: 'e-mailadres',
    numberField: 'nummmer',
    profile: 'profiel foto',
  },
  messages: {
    required: {
      default: 'Uw {fieldName} is verplicht'
    },
    alpha: {
      default: 'Uw {fieldName} mag alleen alfabetten bevatten'
    },
    alphaNumeric: {
      default: 'Uw {fieldName} mag alleen letters en cijfers bevatten'
    },
    numeric: {
      default: 'Uw {fieldName} mag alleen cijfers bevatten'
    },
    email: {
      default: 'Uw {fieldName} is geen geldig e-mailadres'
    },
    ext: {
      default: 'De extensie van uw {fieldName} mag alleen {ext} zijn',
    },
    max: {
      default: 'Uw {fieldName} mag niet groter zijn dan {max}'
    },
    min: {
      default: 'Uw {fieldName} mag niet kleiner zijn dan {min}'
    },
    maxLength: {
      default: 'Uw {fieldName} mag niet langer zijn dan {maxLength} tekens'
    },
    minLength: {
      default: 'Uw {fieldName} mag niet korter zijn dan {minLength} tekens'
    },
    maxSize: {
      default: 'Uw {fieldName} mag niet groter zijn dan {maxSize}MB'
    },
    minSize: {
      default: 'Uw {fieldName} mag niet kleiner zijn dan {minSize}MB'
    },
    passwordConfirm: {
      default: 'Uw wachtwoorden komen niet overeen'
    }
  }
}