import BaseRuleField from "./BaseRuleField.js";

export default class AggressiveRuleField extends BaseRuleField {
  constructor({id, formId, fieldName, rules, type}) {
    super({id, formId, fieldName, rules, type});
    this.listeners = {};
  }

  setFieldData({formData}) {
    super.setFieldData({formData});
    console.log('form data:', this.formData);
    super.validateRules();
  }
}