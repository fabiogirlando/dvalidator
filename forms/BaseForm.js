import {MODES} from "./FormManager.js";
import BaseRuleField from "../fields/BaseRuleField.js";
import LazyRuleFieldProgress from "../fields/LazyRuleFieldProgress.js";

export default class BaseForm {
  constructor(formId, mode, name, onFormDataUpdate) {
    this.id = formId;
    this.mode = mode;
    this.name = name;
    this.fields = {};
    this.formData = {};
    this.onFormDataUpdate = onFormDataUpdate;
    this.isPristine = true;
    this.isDirty = false;
    this.isValid = true;
    this.isInvalid = false;
  }

  setFormData({fieldName, data}) {
    // update the entire form data
    this.formData = Object.assign({}, this.formData, {[fieldName]: data});
    this.isPristine = false;
    this.isDirty = true;
    // update parent if needed
    if (this.onFormDataUpdate) {
      this.onFormDataUpdate(this.formData);
    }
  }

  addRuleField({fieldId, formId, fieldName, rules, type}) {
    if (this.mode === MODES.LAZY) {
      this.fields[fieldId] = new LazyRuleFieldProgress({id: fieldId, formId, fieldName, rules, type});
    } else {
      this.fields[fieldId] = new BaseRuleField({id: fieldId, formId, fieldName, rules, type});
    }
    return {field: this.fields[fieldId]};
  }

  validateField(fieldId) {
    const ruleField = this.fields[fieldId];
    if (ruleField) {
      ruleField.validateRules();
      this.isValid = this.checkFormValid();
      this.isInvalid = !this.isValid;
    }
  }

  checkFormValid() {
    for (const fieldKey in this.fields) {
      if (this.fields.hasOwnProperty(fieldKey)) {
        const field = this.fields[fieldKey];
        if (!field.isValid) {
          return false;
        }
      }
    }
    return true;
  }

  validateFormFields() {
    let isFormValid = true;
    for (const fieldId in this.fields) {
      if (this.fields.hasOwnProperty(fieldId)) {
        const ruleField = this.fields[fieldId];
        // validate rules, e.g. required|length:8|size:5
        if (!ruleField.validateRules()) {
          isFormValid = false;
        }
      }
    }
    this.isValid = isFormValid;
    this.isInvalid = !isFormValid;
    return {isFormValid, formData: this.formData};
  }

  validateFieldByData(fieldId, dataValue) {
    const ruleField = this.fields[fieldId];
    if (ruleField) {
      // make it like a formData format -> key value pair
      const parsedData = {[ruleField.fieldName]: dataValue};
      ruleField.validateRulesByData(parsedData);
    }
  }

  resetFieldErrors() {
    for (const fieldId in this.fields) {
      if (this.fields.hasOwnProperty(fieldId)) {
        const ruleField = this.fields[fieldId];
        ruleField.resetErrors();
      }
    }
  }
}
